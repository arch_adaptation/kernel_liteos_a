#ifdef _ADAPT_TIMESPEC_H
#define _ADAPT_TIMESPEC_H

#define itimerspec __itimerspec_discard
#include_next <sys/timespec.h>
#undef itimerspec

#endif
